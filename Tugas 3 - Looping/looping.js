//Tugas 1
//Algoritma
function looping(max) {
    temp = 0;
    console.log('LOOPING PERTAMA');
    while (temp < max) {
        temp += 2;
        console.log(temp.toString(), "- I love coding");
        
    }
    console.log('LOOPING KEDUA');
    while (temp > 1) {
        console.log(temp.toString(), "- I will become a mobile developer");
        temp -= 2;
    }
}

//hasil
looping(20);
console.log();
//Tugas 2
//algoritma
function looping_2(max) {
    for (i = 1; i <= max; i++){
        if (i % 3 == 0) {
            console.log(i.toString(), "- I Love Coding");
        }
        else if (i % 2 == 1) {
            console.log(i.toString(), "- Santai");
        }
        else {
            console.log(i.toString(), "- Berkualitas");
        }
    }
}

//hasil
looping_2(20);
console.log();

//tugas 3
//algoritma menggunakan repeat
function persegi(lebar,tinggi) {
    for (i = 1; i <= tinggi; i++){
        console.log("#".repeat(lebar));
    }
}
//algoritma biasa
function persegi_2(lebar, tinggi) {
    var temp = "";
    for (i = 1; i <= tinggi; i++){
        for (j = 1; j <= lebar; j++){
            temp += "#";
        }
        console.log(temp);
        temp = "";
    }
}

persegi_2(8, 4);
console.log();
//tugas 4
//algoritma dengan repeat
function tangga(tinggi) {
    for (i = 1; i <= tinggi; i++) {
        console.log("#".repeat(i));
    }
}

//algoritma biasa
function tangga_2(tinggi) {
    temp = "";
    for (i = 1; i <= tinggi; i++){
        for (j = 1; j <= i; j++){
            temp += "#";
        }
        console.log(temp);
        temp = "";
    }
}

//hasil
tangga_2(7);
console.log();

//tugas_5
//algoritma
function catur(ukuran) {
    temp = "";
    for (i = 1; i <= ukuran; i++){
        for (j = 1; j <= ukuran; j++){
            if (i % 2 == 0) {
                if (j % 2 == 0) {
                    temp += " ";
                }
                else temp += "#";
            }
            else {
                if (j % 2 == 0) {
                    temp += "#";
                }
                else temp += " ";
            }
            
        }
        console.log(temp);
        temp = "";
    }
}

catur(8);
