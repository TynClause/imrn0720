// di index.js
var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
    {name: "Machine Learning",timeSpent: 5000}
]

// Tulis code untuk memanggil function readBooks di sini

function rekursif(time, i) {
    readBooks(time, books[i], function (sisa) {
        if (sisa != 0) {
            if (i + 1 < books.length) rekursif(sisa, i + 1)
        }
    })
}

rekursif(10_000, 0)
//rekursif(20_000,0)
