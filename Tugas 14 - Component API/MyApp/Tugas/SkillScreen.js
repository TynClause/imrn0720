import React, { Component } from "react";
import { View, Text, Image, StyleSheet, FlatList } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";

import Data from "./skillData.json";
import Card from "./Card";

export default class SkillScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View>
          <Image
            source={require("./Images/logo_putih 1.png")}
            style={{ width: 187.5, height: 51, left: 187 }}
          />
        </View>

        <View style={styles.user}>
          <Icon style={styles.userIcon} name="user-circle" size={26} />
          <View style={styles.userName}>
            <Text style={{ fontSize: 12, lineHeight: 14 }}>Hai,</Text>
            <Text style={{ fontSize: 16, lineHeight: 19, color: "#003366" }}>
              Elvin Nur Furqon
            </Text>
          </View>
        </View>

        <Text
          style={{
            fontSize: 36,
            lineHeight: 42,
            fontFamily: "Roboto",
            marginLeft: 16,
            color: "#003366",
          }}
        >
          SKILL
        </Text>
        <View
          style={{
            marginLeft: 16,
            marginRight: 16,
            borderBottomWidth: 5,
            borderBottomColor: "#3EC6FF",
          }}
        ></View>

        <View style={styles.mainCategory}>
          <View style={styles.subCategory}>
            <Text
              style={{
                fontSize: 12,
                lineHeight: 14,
                color: "#003366",
                fontWeight: "bold",
                marginTop: 9,
                marginBottom: 9,
                marginLeft: 8,
                marginRight: 8,
              }}
            >
              Library / Framework
            </Text>
          </View>
          <View style={styles.subCategory}>
            <Text
              style={{
                fontSize: 12,
                lineHeight: 14,
                color: "#003366",
                fontWeight: "bold",
                marginTop: 9,
                marginBottom: 9,
                marginLeft: 8,
                marginRight: 8,
              }}
            >
              Bahasa Pemrograman
            </Text>
          </View>
          <View style={styles.subCategory}>
            <Text
              style={{
                fontSize: 12,
                lineHeight: 14,
                color: "#003366",
                fontWeight: "bold",
                marginTop: 9,
                marginBottom: 9,
                marginLeft: 8,
                marginRight: 8,
              }}
            >
              Teknologi
            </Text>
          </View>
        </View>

        <FlatList
          data={Data.items}
          renderItem={(skill) => <Card skill={skill} />}
          keyExtractor={(item) => item.id}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  user: {
    marginTop: 3,
    marginBottom: 16,
    marginRight: 212,
    marginLeft: 16,

    flexDirection: "row",
  },
  userIcon: {
    marginTop: 4,

    marginRight: 11,
    marginLeft: 3,
    color: "#3EC6FF",
  },
  mainCategory: {
    flexDirection: "row",
    marginTop: 10,
    marginBottom: 10,
  },
  subCategory: {
    backgroundColor: "#B4E9FF",
    marginHorizontal: 5,
    borderRadius: 8,
  },
});
