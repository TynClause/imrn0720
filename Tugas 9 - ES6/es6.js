//tugas 1
//old version
/*
const golden = function goldenFunction() {
    console.log("this is golden!!")
}

golden()
*/
//ES6
const golden = () => {
    console.log("this is golden!!")
}

golden()

//tugas 2
//old version
/*
const newFunction = function literal(firstName, lastName) {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: function () {
            console.log(firstName + " " + lastName)
            return
        }
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName()
*/

//ES6
const newFunction = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: () => { console.log(`${firstName} ${lastName}`)}
    }
}

newFunction("William", "Imoh").fullName()


//tugas 3
//old version

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
/*
const firstName = newObject.firstName;
const lastName = newObject.lastName;
const destination = newObject.destination;
const occupation = newObject.occupation;
*/

//ES6

const { firstName, lastName, destination, occupation,_} = newObject

console.log(firstName, lastName, destination, occupation)

//tugas 4
//old version

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
/*
const combined = west.concat(east)
//Driver Code
console.log(combined)
*/
//ES6
const combined = [...west,...east]
console.log(combined)


//tugas 5
//old version
const planet = "earth"
const view = "glass"
/*
var before = 'Lorem ' + view + 'dolor sit amet, ' +
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'

// Driver Code
console.log(before) 
*/


var before = `lorem ${view} dolor sit amet, consectetur adipiscing ` +
    `elit,${planet} do eiusmod tempor incididunt ut labore` +
    `et dolore magna aliqua.Ut enim ad minim veniam`

console.log(before) 