//soal 1
function balikString(kalimat) {
    var hasil = "";
    for (i = kalimat.length - 1; i >= 0; i--) {
        hasil += kalimat[i];
    }
    return hasil;
}
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah


console.log()

//soal 2

function palindrome(kalimat) {
    var temp = kalimat;
    var hasil = "";
    for (i = kalimat.length - 1; i >= 0; i--) {
        hasil += kalimat[i];
    }
    if (temp == hasil) {
        return true
    }
    else {
        return false
    }
}


console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false

console.log()


//soal 3
function bandingkan(number =  null, bilPositif = null) {
    if (number  < 0 || bilPositif < 0 || number == bilPositif || number == null && bilPositif == null) {
        return -1
    }
    else {
        if (number > bilPositif || bilPositif == null) {
            return number;
        }
        else return bilPositif
    }
    
}

// TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18


console.log()

//soal 4
function DescendingTen(number=null) {
    if (number == null) {
        return -1
    }
    else {
        hasil = ""
        counter = 1
        while (counter <= 10) {
            hasil += number.toString() + " "
            number -= 1
            counter+=1
        }
    }
    return hasil;
}
// TEST CASES Descending Ten
console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1
