//Soal 1

// algoritmanya
function werewolf(nama, peran) {
  if (nama == null) {
    return "Nama harus diisi!";
  } else if (peran == null) {
    return "Halo " + nama + ", Pilih peranmu untuk memulai game!";
  } else {
    if (peran.toLowerCase() == "penyihir") {
      return (
        "Halo Penyihir " +
        nama +
        ", kamu dapat melihat siapa yang menjadi werewolf!"
      );
    } else if (peran.toLowerCase() == "guard") {
      return (
        "Halo Guard " +
        nama +
        ", kamu akan membantu melindungi temanmu dari serangan werewolf."
      );
    } else if (peran.toLowerCase() == "werewolf") {
      return (
        "Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!"
      );
    } else return "mana anda jadi " + peran + " di game werewolf bujanx..";
  }
}

//input
var nama = "Elvin";
var peran = "Pemain putsal";
//var peran = "WEREWoft"

//hasil
var hasil = werewolf(nama, peran);
console.log(hasil);

//Soal 2

//algoritmanya
function ubahbulan(bulan) {
  var namaBulan;
  switch (bulan) {
    case 1:
      namaBulan = "Januari";
      break;
    case 2:
      namaBulan = "February";
      break;
    case 3:
      namaBulan = "Maret";
      break;
    case 4:
      namaBulan = "April";
      break;
    case 5:
      namaBulan = "May";
      break;
    case 6:
      namaBulan = "Juni";
      break;
    case 7:
      namaBulan = "July";
      break;
    case 8:
      namaBulan = "Agustus";
      break;
    case 9:
      namaBulan = "September";
      break;
    case 10:
      namaBulan = "Oktober";
      break;
    case 11:
      namaBulan = "November";
      break;
    case 12:
      namaBulan = "Desember";
      break;
    default:
      namaBulan = "";
      break;
  }
  return namaBulan;
}

//input
var hari = 21;
var bulan = 1;
var tahun = 1945;

//hasil
var hasil = ubahbulan(bulan);
if (hasil == "" || hasil == "" || hari == "" || tahun == "") {
  console.log("punten nih, kayaknya ada yang gak beres dengan inputan masbro");
} else if (hasil == null || hari == null || tahun == null) {
  console.log("punten nih, inputan kosong nih...");
} else {
  console.log(hari.toString(), hasil, tahun.toString());
}
