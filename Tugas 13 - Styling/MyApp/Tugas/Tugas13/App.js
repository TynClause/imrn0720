import React from "react";
import {
  View,
  ImageBackground,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from "react-native";

const images = require("./images/gambar.jpg");

const App = () => (
  <View style={styles.container}>
    <ImageBackground source={images} style={styles.image}>
      <Text style={styles.text}>Welcome To {"\n"}My App</Text>
      <View style={styles.box}>
        <text
          style={{
            position: "absolute",
            textAlign: "center",
            fontSize: 24,
            fontFamily: "Roboto",
            marginTop: 14,
            marginBottom: 300,
            marginTop: 79,
            marginLeft: 112,
          }}
        >
          Login
        </text>
        <TextInput
          style={{
            position: "absolute",
            height: 40,
            borderColor: "gray",
            borderWidth: 1,
            width: 210,
            height: 41,
            marginBottom: 168,
            marginTop: 79,
            marginLeft: 35,
          }}
          value=" ID User"
        />
        <TextInput
          style={{
            position: "absolute",
            height: 40,
            borderColor: "gray",
            borderWidth: 1,
            width: 210,
            height: 41,
            marginTop: 160,
            marginBottom: 87,
            marginLeft: 35,
          }}
          value=" Password"
        />
        <TouchableOpacity>
          <Text
            style={{
              color: "284A5E",
              fontSize: 20,
              fontWeight: "bold",
              marginTop: 221,
              marginLeft: 75,
              marginRight: 75,
              marginBottom: 20,
              textAlign: "center",
              backgroundColor: "red",
              borderRadius: 5,
              width: 131.54,
              height: 40,
            }}
          >
            Login
          </Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center",
    opacity: 0.6,
  },
  text: {
    flex: 1,
    color: "284A5E",
    fontSize: 48,
    fontWeight: "bold",
    marginTop: 31,
    marginLeft: 41,
    marginRight: 41,
    textAlign: "center",
  },
  box: {
    backgroundColor: "white",
    marginLeft: 46,
    marginRight: 46,
    marginBottom: 178,
    width: 283,
    height: 288,
    justifyContent: "center",
    borderRadius: 20,
    flexDirection: "column",
    opacity: 0.89,
  },
  tombol: {
    margin: 20,
  },
});

export default App;
