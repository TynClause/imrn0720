//tugas 1
//algoritma
function arrayToObject(arr) {
    hasil = {};
    var now = new Date();
    var thisYear = now.getFullYear();
    for (i = 0; i <= arr.length - 1; i++){
        name = (i+1).toString() + ". " + arr[i][0] + " " + arr[i][1];
        hasil[name] = {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
        }
        if (arr[i].length == 3) {
            hasil[name].age = "Invalid Birth Year";
        }
        else {
            hasil[name].age = (thisYear - arr[i][3]);
        }
    }
    return hasil;
}

var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]];

console.log(arrayToObject(people));
console.log();


//tugas 2
//algoritma
var listbarang = {
    'Sepatu Stacattu': 1_500_000,
    'Baju Zoro': 500_000,
    'Baju H&N': 250_000,
    'Sweater Uniklooh': 175_000,
    'Casing Handphone': 50_000, 
}


function shoppingTime(memberId = null, moneymember = 0) {
    if (memberId == null || memberId == '') {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
    else if (moneymember < 50_000) {
        return "Mohon maaf, uang tidak cukup"
    }
    else {
        list = []
        dict = {
            memberId: memberId,
            money: moneymember,
        }
        for (key in listbarang) {
            if (moneymember >= listbarang[key]) {
                list.push(key);
                moneymember -= listbarang[key];
            }
        }
        dict.listPurchased = list;
        dict.changeMoney = moneymember;
    }
    return dict;
}

console.log(shoppingTime('82Ku8Ma742', 170_000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log()
//soal 3
//algoritma

function naikAngkot(listPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    list = []
    for (penumpang_ in listPenumpang) {
        dict = {
            penumpang: listPenumpang[penumpang_][0],
            naikDari: listPenumpang[penumpang_][1],
            tujuan: listPenumpang[penumpang_][2],
        }
        flag = false;
        harusdibayar = 0;
        for (var terminal in rute) {
            if (rute[terminal] == dict.naikDari) {
                flag = true;
            }
            if (rute[terminal] == dict.tujuan) {
                break;
            }
            if (flag) {
                harusdibayar += 2000;
            }
        }
        dict.bayar = harusdibayar;
        list.push(dict);
    }
    return list;
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([])); 