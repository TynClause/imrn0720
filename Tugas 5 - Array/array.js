//Tugas 1
//algoritma
function range(startNum = null, finishNum = null) {
  if (startNum == null || finishNum == null) {
    return -1;
  } else {
    hasil = [];
    if (startNum > finishNum) {
      while (startNum >= finishNum) {
        hasil.push(startNum);
        startNum -= 1;
      }
    } else if (startNum < finishNum) {
      while (startNum <= finishNum) {
        hasil.push(startNum);
        startNum += 1;
      }
    } else {
      while (startNum >= finishNum) {
        hasil.push(startNum);
        startNum -= 1;
      }
    }
  }
  return hasil;
}

console.log(range(40, 50));
console.log();

//tugas 2
//algoritma
function rangeWithStep(startNum = null, finishNum = null, step = 1) {
  //step saya ubah menjadi nilai positif semua untuk menghindari error
  if (startNum == null || finishNum == null || step <= 0) {
    return -1;
  } else {
    hasil = [];
    if (startNum > finishNum) {
      while (startNum >= finishNum) {
        hasil.push(startNum);
        startNum -= step;
      }
    } else if (startNum < finishNum) {
      while (startNum <= finishNum) {
        hasil.push(startNum);
        startNum += step;
      }
    } else {
      while (startNum >= finishNum) {
        hasil.push(startNum);
        startNum -= 1;
      }
    }
  }
  return hasil;
}

console.log(rangeWithStep(41, 50, 4));
console.log();

//tugas 3
//algoritma
function sum(startNum = null, finishNum = null, step = 1) {
  if (step <= 0) {
    return "error";
  }
  //step saya ubah menjadi nilai positif semua untuk menghindari error
  if (startNum == null && finishNum == null) {
    return 0;
  } else {
    if (finishNum == null) {
      finishNum = startNum;
    }
    hasil = [];
    if (startNum > finishNum) {
      while (startNum >= finishNum) {
        hasil.push(startNum);
        startNum -= step;
      }
    } else if (startNum < finishNum) {
      while (startNum <= finishNum) {
        hasil.push(startNum);
        startNum += step;
      }
    } else {
      while (startNum >= finishNum) {
        hasil.push(startNum);
        startNum -= 1;
      }
    }
  }
  var sum = hasil.reduce(function (a, b) {
    return a + b;
  }, 0);
  return sum;
}

console.log(sum(5, 50, 2));
console.log();

//Tugas 4
//Algoritma
function dataHandling(input_data) {
  for (i = 0; i < input_data.length; i++) {
    for (j = 0; j < input_data[i].length; j++) {
      switch (j) {
        case 0:
          console.log("Nomor ID:", input_data[i][j]);
          break;
        case 1:
          console.log("Nama Lengkap:", input_data[i][j]);
          break;
        case 2:
          console.log("TTL:", input_data[i][j]);
          break;
        case 3:
          console.log("Hobi:", input_data[i][j]);
          break;
        default:
          console.log("Data Tambahan :", input_data[i][j]);
      }
    }
    console.log("\n");
  }
}

//hasil
var inputdata = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
];

dataHandling(inputdata);

//Tugas 5
//algoritma
function balikKata(string) {
  hasil = "";
  for (i = string.length - 1; i >= 0; i--) {
    hasil += string[i];
  }
  return hasil;
}

console.log(balikKata("SanberCode"));
console.log();

//Tugas 6
//algoritma
function dataHandling2(array) {
  array.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
  array.splice(4, 1, "Pria", "SMA Internasional Metro");
  console.log(array);
  bulan = parseInt(array[3][3] + array[3][4]); 
  switch (bulan) {
    case 1: bulan= "January"; break;
    case 2: bulan= "February"; break;
    case 3: bulan= "Maret"; break;
    case 4: bulan= "April"; break;
    case 5: bulan= "Mei"; break;
    case 6: bulan= "Juni"; break;
    case 7: bulan= "Juli"; break;
    case 8: bulan= "Agustus"; break;
    case 9: bulan= "September"; break;
    case 10:bulan = "Oktober"; break;
    case 11:bulan = "November"; break;
    case 12:bulan = "Desember"; break;
  }
  console.log(bulan);
  tgl = array[3];
  tgl = tgl.split("/");
  tgl.sort(function (a, b) { return b - a });
  console.log(tgl);
  tgl_2 = array[3];
  tgl_2 = tgl_2.split("/");
  tgl_2 = tgl_2.join("-");
  console.log(tgl_2);
  nama = array[1];
  nama = nama.slice(0, 15);
  console.log(nama);
}

var data = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"] 

dataHandling2(data);
console.log();