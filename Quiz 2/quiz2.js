/*
  SOAL CLASS SCORE (15 poin + 5 es6)
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email".
  "points" dapat di input berupa number atau array of number.
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/
//tuliskan coding disini
class score {
  constructor(points) {
    this.points = points;
  }
  average = () => {
    var hasil = this.points.reduce((a, b) => a + b, 0);
    return hasil;
  };
}

var test = new score([1, 2, 3]);
console.log(test.average());

//end

/*
  SOAL View Score (15 Poin + 5 Poin ES6)
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh:
  Input

  data :
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]

  subject: "quiz-1"

  Output
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/
//tuliskan coding disini
var viewScores = (subject, data) => {
  var list = [];
  var dict = {};
  for (i = 1; i < data.length; i++) {
    dict.email = data[i][0];
    dict.subject = subject;
    for (j = 1; j < data[i].length - 1; j++) {
      if (subject == data[0][j]) {
        index = j;
        break;
      }
    }
    dict.points = data[i][index];
    list.push(dict);
    dict = {};
  }
  return list;
};

var nama = "quiz-1";

var data = [
  ["email", "quiz-1", "quiz-2", "quiz-3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
];

console.log(viewScores(nama, data));
//end

/*
  SOAL Recap Score (20 Poin + 5 Poin ES6)
    buatlah function recapScore menampilkan perolehan nilai semua student.
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan.
    Predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    contoh output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/
//tuliskan code anda disini

function recapScore(data) {
  var list = [];
  var dict = {};
  total = 0;
  counter = 0;
  for (i = 1; i < data.length; i++) {
    dict.email = data[i][0];
    for (j = 1; j < data[i].length - 1; j++) {
      total += data[i][j];
      counter += 1;
    }
    var nilai = total / counter;
    dict.points = nilai;
    if (nilai > 90) {
      dict.predikat = "honour";
    } else if (nilai > 80) {
      dict.predikat = "graduate";
    } else if (nilia > 70) {
      dict.predikat = "participant";
    } else {
      dict.predikat = "unpredicate";
    }

    list.push(dict);
    dict = {};
  }

  for (i = 0; i < list.length; i++) {
    console.log(
      `${i + 1}. Email: ${list[i]["email"]}\nRata - rata : ${
        list[i]["points"]
      }\nPredikat: ${list[i]["predikat"]}`
    );
  }
}

//end
recapScore(data);

//rekap menjadi 1  dan menjadi ES6

class Score {
  constructor(subject, points, data) {
    this.subject = subject;
    this.points = points;
    this.data = data;
  }
  average = () => {
    var hasil = this.points.reduce((a, b) => a + b, 0);
    return hasil;
  };

  viewScores = () => {
    var list = [];
    var dict = {};
    for (i = 1; i < this.data.length; i++) {
      dict.email = this.data[i][0];
      dict.subject = subject;
      for (j = 1; j < this.data[i].length - 1; j++) {
        if (this.subject == data[0][j]) {
          index = j;
          break;
        }
      }
      dict.points = this.data[i][index];
      list.push(dict);
      dict = {};
    }
    return list;
  };

  recapScore = () => {
    var list = [];
    var dict = {};
    total = 0;
    counter = 0;
    for (i = 1; i < this.data.length; i++) {
      dict.email = this.data[i][0];
      for (j = 1; j < this.data[i].length - 1; j++) {
        total += this.data[i][j];
        counter += 1;
      }
      var nilai = total / counter;
      dict.points = nilai;
      if (nilai > 90) {
        dict.predikat = "honour";
      } else if (nilai > 80) {
        dict.predikat = "graduate";
      } else if (nilia > 70) {
        dict.predikat = "participant";
      } else {
        dict.predikat = "unpredicate";
      }

      list.push(dict);
      dict = {};
    }

    for (i = 0; i < list.length; i++) {
      console.log(
        `${i + 1}. Email: ${list[i]["email"]}\nRata - rata : ${
          list[i]["points"]
        }\nPredikat: ${list[i]["predikat"]}`
      );
    }
  };
}
